const path = require('path')

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: './src/customMaterialConfigurator.ts',
    devServer: {
        hot: false,
        port: 9103,
        static: {
            serveIndex: true,
            directory: __dirname
        },
        devMiddleware: {
            writeToDisk: true,
        },
        client: false,
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist/'
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            // {
            // test: /\.js$/,
            // enforce: "pre",
            // use: ["source-map-loader"],
            // },
        ]
    },
    watchOptions: {
        ignored: /node_modules/
    }
}
