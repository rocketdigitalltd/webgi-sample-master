; (function ($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);

	$doc.ready(function () {
		$(".nav-item-left").click(function (event) {
			$(".nav-item-left").find('.triangle-inner').removeClass('light-blue');
			$(".nav-item-middle").find('.triangle-right').removeClass('light-blue');
		});

		$(".nav-item-middle").click(function (event) {
			$(".nav-item-left").find('.triangle-inner').addClass('light-blue');
			$(".nav-item-middle").find('.triangle-left').addClass('light-blue');
			$(".nav-item-middle").find('.triangle-right').removeClass('light-blue');
			$(".nav-item-right").find('.triangle-left-first').removeClass('light-blue');
		});

		$(".nav-item-right").click(function (event) {
			$(".nav-item-left").find('.triangle-inner').removeClass('light-blue');
			$(".nav-item-middle").find('.triangle-inner').addClass('light-blue');
			$(".nav-item-right").find('.triangle-inner').addClass('light-blue');
		});

		$('[data-fancybox="gallery"]').fancybox({
			protect: true
		})
	});
})(jQuery, window, document);
