# WebGi Sample

## Installing dependencies

    npm i

## Running the project in watch mode

    npm start

    Note::
        To run a specific example, open webpack.config.js and enter the file name in the 'entry' field.(default entry: './src/hellopixo.ts'). After that call npm start

## Building the project

    npm run build
