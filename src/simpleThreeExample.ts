import {
    AssetManagerPlugin,
    TonemapPlugin,
    ViewerApp,
    THREE,
    addFBXLoader,
} from "webgi";

import { IModel, ITexture } from "webgi/dist/src/interfaces";


const viewer = new ViewerApp({
    canvas: document.getElementById('main-canvas') as HTMLCanvasElement,
    useRgbm: true, // Use HDR RGBM Pipeline. false will use HDR Half float pipeline
})

async function buildScene(viewer: ViewerApp) {
    const rootNode = new THREE.Group()
    // Assign some material properties
    const cube = <THREE.Mesh>viewer.scene.findObjectsByName('Cube')[0]
    if(cube) {
        let cubeMaterial = <THREE.MeshStandardMaterial>cube.material;
        cubeMaterial.color = new THREE.Color(0xff0000)
        cubeMaterial.metalness = 1
        cubeMaterial.roughness = 0.1
    }
    rootNode.add(cube);

    // Add custom three.js geometry, lights
    const sphereMesh = new THREE.Mesh(new THREE.SphereBufferGeometry(1))
    let material: THREE.MeshStandardMaterial = new THREE.MeshStandardMaterial({roughness: 0.3, metalness: 1, color: new THREE.Color(0xffff00)});
    sphereMesh.material = material;

    sphereMesh.position.x = 2
    sphereMesh.scale.set(0.5, 0.5, 0.5)
    rootNode.add(sphereMesh)

    const planeMesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(10, 10, 10))
    planeMesh.rotation.x = -Math.PI/2
    rootNode.add(planeMesh)

    // Load fbx node
    const fbxNode = <IModel> await viewer.getManager()?.importer?.importSinglePath('https://demo-assets.pixotronics.com/pixo/fbx/testFbx.fbx', {})
    fbxNode.modelObject.position.x = -2
    rootNode.add(fbxNode.modelObject)

    const light = new THREE.DirectionalLight()
    light.intensity = 2
    light.shadow.bias = -0.00004
    light.shadow.mapSize = new THREE.Vector2(1024, 1024)
    light.castShadow = true
    light.position.set(5, 5, 5)
    rootNode.add(light)

    return rootNode
}

async function initialize() {

    const manager = await viewer.addPlugin(new AssetManagerPlugin())
    await viewer.addPlugin(new TonemapPlugin)

    addFBXLoader()

    // Sets up the render pipeline. This also adds some dependencies based on which Plugins are added to the viewer
    viewer.renderer.refreshPipeline()

    const envTexture: ITexture | undefined = await manager?.importer?.importSingle({path:'https://demo-assets.pixotronics.com/pixo/hdr/p360-01.hdr'})
    await viewer.scene.setEnvironment(envTexture)

    await manager.addAsset({path:'https://demo-assets.pixotronics.com/pixo/gltf/cube.glb'})

    const rootNode = await buildScene(viewer)
    await manager.addImported(rootNode, {autoScale: false})

    // Change position of the active camera
    viewer.scene.activeCamera.position.y += 6
    viewer.scene.activeCamera.positionUpdated()

    // Add an event listener 'preFrame' which is called every frame before the scene is rendered
    viewer.addEventListener('preFrame', ()=> {
        const cube = <THREE.Mesh>viewer.scene.findObjectsByName('Cube')[0]
        if(cube) {
            cube.rotation.y += 0.01
            // Make sure to call setDirty to indicate a change in the scene
            cube.userData.setDirty()
        }
    })
}

initialize()
