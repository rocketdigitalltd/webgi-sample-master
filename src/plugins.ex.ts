import {
    AssetManagerPlugin,
    BloomPlugin,
    CameraViewPlugin,
    DiamondPlugin,
    GroundPlugin,
    ProgressivePlugin,
    SSAOPlugin,
    SSRPlugin,
    TemporalAAPlugin,
    TonemapPlugin,
    ViewerApp
} from "webgi";

const viewer = new ViewerApp({
    canvas: document.getElementById('main-canvas') as HTMLCanvasElement,
    useRgbm: true, // Use HDR RGBM Pipeline. false will use HDR Half float pipeline
})

async function initialize() {

    const manager = await viewer.addPlugin(AssetManagerPlugin)

    await viewer.addPlugin(new ProgressivePlugin(16))
    await viewer.addPlugin(TonemapPlugin)
    await viewer.addPlugin(SSRPlugin)
    await viewer.addPlugin(DiamondPlugin)
    await viewer.addPlugin(GroundPlugin)
    await viewer.addPlugin(BloomPlugin)
    await viewer.addPlugin(SSAOPlugin)
    await viewer.addPlugin(TemporalAAPlugin)
    await viewer.addPlugin(CameraViewPlugin)

    // Sets up the render pipeline. This also adds some dependencies based on which Plugins are added to the viewer
    viewer.renderer.refreshPipeline()

    await viewer.getManager()?.addAsset({path:'https://demo-assets.pixotronics.com/pixo/gltf/testRingScene.glb'})
}

initialize()
