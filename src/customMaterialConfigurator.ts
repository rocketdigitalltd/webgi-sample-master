import {
    AssetManagerPlugin,
    BloomPlugin,
    DiamondPlugin,
    ProgressivePlugin,
    SSAOPlugin,
    SSRPlugin,
    TemporalAAPlugin,
    TonemapPlugin,
    ViewerApp, domHelpers, GroundPlugin, MaterialConfiguratorPlugin, FrameFadePlugin, MaterialConfiguratorBasePlugin
} from "webgi";

export const makeColorSvg = (color: string|number): string => {
    return `data:image/svg+xml,%3Csvg width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Crect width='16' height='16' fill='%23${color}'/%3E%3C/svg%3E%0A`
}

const viewer = new ViewerApp({
    canvas: document.getElementById('main-canvas') as HTMLCanvasElement,
    useRgbm: true, // Use HDR RGBM Pipeline. false will use HDR Half float pipeline
})

async function initialize() {
    const manager = await viewer.addPlugin(AssetManagerPlugin)

    await viewer.addPlugin(new ProgressivePlugin(16))
    await viewer.addPlugin(TonemapPlugin)
    await viewer.addPlugin(SSRPlugin)
    await viewer.addPlugin(DiamondPlugin)
    await viewer.addPlugin(BloomPlugin)
    await viewer.addPlugin(SSAOPlugin)
    await viewer.addPlugin(FrameFadePlugin)
    await viewer.addPlugin(TemporalAAPlugin)
    await viewer.addPlugin(GroundPlugin)

    // to access go to http://localhost:9103/?default
    if(domHelpers.getUrlQueryParam('default') != null) {

        // this will load the default UI.
        const configurator = await viewer.addPlugin(MaterialConfiguratorPlugin)
    } else {

        // custom implementation, check the console logs after running.
        const configurator = await viewer.addPlugin(class extends MaterialConfiguratorBasePlugin{

            public static PluginType = 'MaterialConfiguratorPlugin'

            // this function is automatically called when an object is loaded with some material variations
            protected async _refreshUi(): Promise<boolean> {
                if (!await super._refreshUi()) return false // check if any data is changed.

                let variationTabs = document.getElementById("nav-tabs-inner");
                let materialTabs = document.getElementById("material_tabs");
                let buttonAutoRotate = document.getElementById('autorotate');

                for (const variation of this.variations) {
                    let li = document.createElement('li');
                    li.classList.add('nav-item');
                    li.setAttribute('role', 'presentation');

                    let a = document.createElement('a');
                    a.classList.add('nav-link');
                    a.setAttribute('id','tab-'+variation.uuid);
                    a.setAttribute('data-toggle','tab');
                    a.setAttribute('href','#'+variation.uuid);
                    a.setAttribute('role','tab');
                    a.appendChild(document.createTextNode(variation.title));

                    li.appendChild(a);

                    variationTabs.appendChild(li);

                    let tabPane  = document.createElement('div');
                    tabPane.classList.add('tab-pane'); // fade show active classes
                    tabPane.setAttribute('id', variation.uuid);
                    tabPane.setAttribute('role', 'tabpanel');

                    let tabProductTeaser = document.createElement('div');
                    tabProductTeaser.classList.add('tab-product-teaser');
                    tabProductTeaser.setAttribute('id', 'teaser_'+variation.uuid);

                    tabPane.appendChild(tabProductTeaser);
                    materialTabs.appendChild(tabPane);

                    let allTabPanes = document.getElementsByClassName('tab-pane');
                    let firstTabPane = allTabPanes[0];
                    firstTabPane.classList.add('fade', 'show', 'active');

                    let allNavLinks = document.getElementsByClassName('nav-link');
                    let firstNavLink = allNavLinks[0];
                    firstNavLink.classList.add('active');

                    variation.materials.map(material => {
                        let image: any
                        if (!variation.preview.startsWith('generate:')) {
                            const pp = (material as any)[variation.preview] || '#ff00ff'
                            // image = pp.image || pp
                            image = pp.image ? domHelpers.imageBitmapToBase64(pp.image, 100) : undefined

                            let hex = rgbToHex(Math.round(pp.r*255), Math.round(pp.g*255), Math.round(pp.b*255));
                            if (!image) image = makeColorSvg(hex)

                            // console.log(pp.r, );
                        } else {
                            // Generate a small snapshot of the material preview based on some shape (optional)
                            image = this._previewGenerator!.generate(material, variation.preview.split(':')[1] as any)
                        }

                        const onClick = ()=>{
                            this.applyVariation(variation, material.uuid)
                        }

                        // Generate a UI from this data.
                        let materialObj = {uid: material.uuid, color: (material as any).color, material: material, image, variation};
                        console.log(materialObj);

                        let materialsContainer = document.getElementById('teaser_'+variation.uuid);
                        let d = document.createElement('label');
                        d.classList.add('tab-product-checkbox');

                        let input = document.createElement('input');
                        input.setAttribute('type', 'checkbox');
                        input.classList.add('checkbox_check');

                        let img = document.createElement('img');
                        let p = document.createElement('p');
                        img.src = image;
                        // d.style.backgroundColor = ('rgb('+image.r+','+image.g+','+image.b);

                        d.appendChild(input);
                        d.appendChild(img);
                        p.appendChild(document.createTextNode(material.name));
                        p.classList.add('tab-product-teaser-text');
                        d.appendChild(p);
                        materialsContainer.appendChild(d);

                        d.addEventListener('click',
                            onClick.bind(variation)
                        );

                        const testFunction2 = ()=>{
                            let elems = document.querySelectorAll('.item-checked');
                            [].forEach.call(elems, function(el) {
                                el.classList.remove("item-checked");
                            });

                            img.classList.add('item-checked');
                        }

                        d.addEventListener('click',
                            testFunction2.bind(d)
                        );
                    })
                }

                let allNavItems = document.getElementsByClassName('nav-item')
                let firstNavItem = allNavItems[0];

                let triangleTopDiv = document.createElement('div');
                triangleTopDiv.classList.add('triangle-inner', 'triangle-right', 'triangle-right-first', 'unclick', 'active-left');

                let triangleTopSpan = document.createElement('span');
                triangleTopSpan.classList.add('triangle-border-top');

                let triangleBottomDiv = document.createElement('div');
                triangleBottomDiv.classList.add('triangle-inner', 'triangle-right', 'triangle-right-second', 'unclick', 'active-left');

                let triangleBottomSpan = document.createElement('span');
                triangleBottomSpan.classList.add('triangle-border-bottom');

                firstNavItem.classList.add('nav-item-left');
                firstNavItem.appendChild(triangleTopDiv);
                firstNavItem.appendChild(triangleTopSpan);
                firstNavItem.appendChild(triangleBottomDiv);
                firstNavItem.appendChild(triangleBottomSpan);

                let secondNavItem = allNavItems[1];
                secondNavItem.classList.add('nav-item-middle');

                // let thirdNavItem = allNavItems[2];
                // thirdNavItem.classList.add('nav-item-right');

                buttonAutoRotate.addEventListener('click', autoRotate);

                return true
            }
        })
    }

    function autoRotate() {
        let rotateStatus = viewer.scene.activeCamera.controls.autoRotate;

        if (rotateStatus == false)
            rotateStatus = true;
        else
            rotateStatus = false;

        viewer.scene.activeCamera.controls.autoRotate = rotateStatus;
    }
    // Sets up the render pipeline. This also adds some dependencies based on which Plugins are added to the viewer
    viewer.renderer.refreshPipeline()

    await viewer.getManager()?.addFromPath('https://demo-assets.pixotronics.com/pixo/gltf/solitaire_config_test.glb')
}

initialize()

function componentToHex(c: any) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r: any, g: any, b: any) {
  return componentToHex(r) + componentToHex(g) + componentToHex(b);
}
