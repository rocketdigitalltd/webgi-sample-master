import {
    AssetManagerPlugin,
    BloomPlugin,
    DiamondPlugin,
    ProgressivePlugin,
    SSAOPlugin,
    SSRPlugin,
    TemporalAAPlugin,
    TonemapPlugin,
    ViewerApp,
    THREE
} from "webgi";
import { ITexture } from "webgi/dist/src/interfaces";

const viewer = new ViewerApp({
    canvas: document.getElementById('main-canvas') as HTMLCanvasElement,
    useRgbm: true, // Use HDR RGBM Pipeline. false will use HDR Half float pipeline
})

async function initialize() {

    const manager = await viewer.addPlugin(AssetManagerPlugin)

    await viewer.addPlugin(new ProgressivePlugin(16))
    await viewer.addPlugin(TonemapPlugin)
    await viewer.addPlugin(SSRPlugin)
    await viewer.addPlugin(DiamondPlugin)
    await viewer.addPlugin(BloomPlugin)
    await viewer.addPlugin(SSAOPlugin)
    await viewer.addPlugin(TemporalAAPlugin)

    // Sets up the render pipeline. This also adds some dependencies based on which Plugins are added to the viewer
    viewer.renderer.refreshPipeline()

    await viewer.getManager()?.addAsset({path:'https://demo-assets.pixotronics.com/pixo/gltf/testMakeDiamond.glb'})
    const envTextureDiamond: ITexture | undefined = await manager?.importer?.importSingle({path:'https://demo-assets.pixotronics.com/pixo/hdr/gem_2.hdr'})
    await viewer.scene.setEnvironment(envTextureDiamond)
    
    const diamondMesh = viewer.scene.findObjectsByName('Diamond_Round')
    if(diamondMesh && diamondMesh[0] instanceof THREE.Mesh) {
        let material = diamondMesh[0].material;
        viewer.getPlugin(DiamondPlugin)?.makeDiamond(material, {}, {})
    }
}

initialize()
