import {
    AssetManagerPlugin,
    TonemapPlugin,
    ViewerApp
} from "webgi";
import { ITexture } from "webgi/dist/src/interfaces";


const viewer = new ViewerApp({
    canvas: document.getElementById('main-canvas') as HTMLCanvasElement,
    useRgbm: true, // Use HDR RGBM Pipeline. false will use HDR Half float pipeline
})

async function initialize() {

    const manager = await viewer.addPlugin(new AssetManagerPlugin())
    await viewer.addPlugin(TonemapPlugin)

    // Sets up the render pipeline. This also adds some dependencies based on which Plugins are added to the viewer
    viewer.renderer.refreshPipeline()

    await Promise.all([
        viewer.scene.setEnvironment(await manager?.importer?.importSingle({path:'https://demo-assets.pixotronics.com/pixo/hdr/p360-01.hdr'})),
        manager?.addAsset({path:'https://demo-assets.pixotronics.com/pixo/gltf/cube.glb'})
    ])

}

initialize()
